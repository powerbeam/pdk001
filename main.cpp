// g++ -o main.exe main.cpp -L. -llibMPSSE

/* INCLUDE FILES */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <sstream>
#include <string>
#include <windows.h>         /* OS specific libraries */
#include "ftd2xx.h"         /* Include D2XX header*/
#include "libMPSSE_i2c.h"   /* Include libMPSSE header */

#define SLAVE_1 0x01;
#define SLAVE_2 0x02;
#define SLAVE_3 0x03;
#define START   0x01;

using namespace std;

/* GLOBAL VARIABLES */
vector<vector<string>> dataList;
FT_HANDLE i2cHandle;						/* System handle that is used throughout the program to select the correct device. */
FT_STATUS i2cStatus = 0;					/* Error codes returned by both the libMPSSE and D2XX calls. */
ChannelConfig i2cChannel;
FT_DEVICE_LIST_INFO_NODE i2cChannelInfo[4];
uint32 i2cNumChannels = 0;
uint32 i2cSlave1Address;
uint8 index;
unsigned char i2cData[260];

uint32 frequency;
uint8 test,equence;

/* PUBLIC FUNCTION DEFINITIONS */

/*
Write Byte is handled directly through a single libMPSSE I2C_DeviceWrite call. An address is specified
along with a data buffer containing two bytes: “command” and “data”. The resulting transmission sends
these three bytes in order, with an ACK/NACK response from the MAX17061 between each one. The
entire transmission is flanked by a START condition at the beginning and a STOP condition at the end. 
*/
FT_STATUS write_bytes(uint8 slaveAddress, const uint8 *data, uint32 numBytes)
{
	uint32 bytesToTransfer = 0;
	uint32 bytesTransfered = 0;
	uint32 bytesToReceive = 0;
	uint32 bytesReceived = 0;
	uint32 options = 0x00;	// I2C_TRANSFER_OPTIONS_BREAK_ON_NACK|I2C_TRANSFER_OPTIONS_START_BIT|I2C_TRANSFER_OPTIONS_STOP_BIT
	
	memcpy(i2cData, data, numBytes);
	bytesToTransfer += numBytes;
	
	printf("Bytes to transfer: %d\n",bytesToTransfer);
	/* for (int i=0;i<numBytes/3;i++)
	{
		printf("R: %d G: %d B: %d\n",unsigned(i2cData[3*i]),unsigned(i2cData[3*i+1]),unsigned(i2cData[3*i+2]));
	}*/
	i2cStatus = I2C_DeviceWrite(i2cHandle, slaveAddress, bytesToTransfer, i2cData, &bytesTransfered, options);
	printf("Transference status: %d\n",i2cStatus);
	if(i2cStatus != FT_OK)
		printf("Bytes not transfered: %d\n",bytesToTransfer-bytesTransfered);
	return i2cStatus;
}

vector<string> split_string(string line, char delimeter)
{
    stringstream ss(line);
    string item;
    vector<string> split_line;
    while (getline(ss, item, delimeter))
    {
       split_line.push_back(item);
    }
    return split_line;
}

void get_data()
{
	ifstream fin;
	string row;
	cout << "Retreiving data..." << endl;
	fin.open("data.csv");
	while (getline(fin, row))
	{
		vector<string> vec = split_string(row,',');
		dataList.push_back(vec);
	}
	/* for(vector<string> vec : dataList)
	{
		for(string data : vec)
		{
			cout<< data << " ";
		}
		cout << endl;
	} */
	cout << "Data retreived." << endl;
}

int main(int argc, char *argv[]) {    
    cout << "C++ Application to communicate using I2C transfer protocol" << endl;
	
	// First, find out how many devices are capable of MPSSE commands
	i2cStatus = I2C_GetNumChannels(&i2cNumChannels);
	if (i2cStatus) 										// Any value besides zero is an error
	{
		printf("ERROR %X - Cannot execute I2C_GenNumChannels. \nPress <Enter> to continue.\n", i2cStatus);
		getchar();
		return 1;
	}
	if (i2cNumChannels < 1)
	{
		printf("No C232HM cables were found.\nPress <Enter> to continue.\n");
		getchar();
		return 1;
	}
	else
	{
		cout << "Channels found: " << i2cNumChannels << endl;
	}
	
	// Next, get the information about each one, ideally there will be only one
	for (int i=0; i < i2cNumChannels; i++)
	{
		i2cStatus = I2C_GetChannelInfo(i, &i2cChannelInfo[i]);	// Get the information from each MPSSE device
		if (i2cStatus)
		{
			printf("ERROR %X - Cannot execute I2C_GetChannelInfo. \nPress <Enter> to continue.\n", i2cStatus);
			getchar();
			return 1;
		}

		printf("Device: %d \nDescription: %s\n", i,i2cChannelInfo[i].Description);
		index = i;
	}

	// We found the cable, now let's open it
	i2cStatus = I2C_OpenChannel(index, &i2cHandle);
	if (i2cStatus)
	{
		printf("ERROR %X - Cannot execute I2C_OpenChannel. \nPress <Enter> to continue.\n",i2cStatus);
		getchar();
		return 1;
	}
	cout << "Openning I2C channel..." << endl;

	// Set up the various I2C parameters
	i2cChannel.ClockRate = I2C_CLOCK_STANDARD_MODE; 	// 100Kbps
	i2cChannel.LatencyTimer = 1; 						// 1mS latency timer
	i2cChannel.Options = 0;
	i2cStatus = I2C_InitChannel(i2cHandle, &i2cChannel);
	if (i2cStatus)
	{
		printf("ERROR %X - Cannot execute I2C_InitChannel. \nPress <Enter> to continue.\n",i2cStatus);
		getchar();
		return 1;

	}
	cout << "I2C configuration done." << endl;

	get_data();

	uint32 frequency = ((uint32)stoi(dataList.at(256).at(0)))/3;
	printf("\nOriginal frequency: %d \nCorrected frequency: %d", (uint32)stoi(dataList.at(256).at(0)), frequency);
	uint8 test = (uint8)stoi(dataList.at(257).at(0));
	uint8 sequence = (uint8)stoi(dataList.at(258).at(0));
	uint8 sequence_hex = 0x00, test_hex = 0x00;

	uint32 bytesTransfered;
	uint32 slaveAddress;
	uint32 bytesToTransfer;
	uint32 options = I2C_TRANSFER_OPTIONS_START_BIT|I2C_TRANSFER_OPTIONS_STOP_BIT; // I2C_TRANSFER_OPTIONS_BREAK_ON_NACK
	
	// --------------- FIRST FPGA --------------- //
	printf(" \n-------------- FPGA 1 --------------\n");
	bytesTransfered = 0;
	slaveAddress = SLAVE_1;
	bytesToTransfer = 1 + 255 + 3; 		// 1 memory address + 255 rgb values + 3 frequency bytes = 259 bytes
	uint8 buffer[bytesToTransfer];
	buffer[0] = 0x00;							/* Byte addressed inside FPGA */
	// printf("Memory address: %d\n",unsigned(buffer[0]));
	cout << "    R | G | B";
	for(int j=0;j<85;j++)
	{
		for (int i=0;i<3;i++)
		{
			buffer[j*3+i+1] = (uint8)stoi(dataList.at(j).at(i));
		}
		printf("\n%d: %d|%d|%d",j,unsigned(buffer[j*3+1]),unsigned(buffer[j*3+1+1]),unsigned(buffer[j*3+2+1]));
	}
	buffer[256] = (uint8)(frequency>>16);
	buffer[257] = (uint8)(frequency>>8);
	buffer[258] = (uint8)(frequency>>0);
	printf("\nFrequency: %d, \nFrequency (hex): %x %x %x\n",frequency,buffer[256],buffer[257],buffer[258]);

	printf("Slave address (hex): %x", slaveAddress);
	printf("\nBytes to transfer: %d", bytesToTransfer);
	i2cStatus = I2C_DeviceWrite(i2cHandle, slaveAddress, bytesToTransfer, buffer, &bytesTransfered, options);
	printf("\nTransfer status: %d", i2cStatus);
	printf("\nBytes transfered: %d", bytesTransfered);
	// Sleep(1000);
	
	// --------------- SECOND FPGA --------------- //
	printf(" \n-------------- FPGA 2 --------------\n");
	bytesTransfered = 0;
	slaveAddress = SLAVE_2;
	bytesToTransfer = 1 + 255 + 3 + 1; 		// 1 memory address + 255 rgb values + 3 frequency bytes + 1 configuration = 260 bytes
	buffer[bytesToTransfer];
	buffer[0] = 0x00;							/* Byte addressed inside FPGA */
	// printf("Memory address: %d\n",unsigned(buffer[0]));
	cout << "    R | G | B";
	for(int j=85;j<170;j++)
	{
		for (int i=0;i<3;i++)
		{
			buffer[(j-85)*3+i+1] = (uint8)stoi(dataList.at(j).at(i));
		}
		printf("\n%d: %d|%d|%d",j,unsigned(buffer[(j-85)*3+1]),unsigned(buffer[(j-85)*3+2]),unsigned(buffer[(j-85)*3+3]));
	} 
	buffer[256] = (uint8)(frequency>>16);
	buffer[257] = (uint8)(frequency>>8);
	buffer[258] = (uint8)(frequency>>0);
	printf("\nFrequency: %d, \nFrequency (hex): %x %x %x",frequency,buffer[256],buffer[257],buffer[258]);

	if(sequence)
		sequence_hex = 0x04;
	if(test)
		test_hex = 0x02;
	buffer[259] = sequence_hex|test_hex;
	printf("\nConfiguration (hex):\n    Sequence: %x\n    Test: %x\n    Byte: %x\n",sequence_hex,test_hex,buffer[259]);

	printf("\nSlave address (hex): %x", slaveAddress);
	printf("\nBytes to transfer: %d", bytesToTransfer);
	i2cStatus = I2C_DeviceWrite(i2cHandle, slaveAddress, bytesToTransfer, buffer, &bytesTransfered, options);
	printf("\nTransfer status: %d", i2cStatus);
	printf("\nBytes transfered: %d", bytesTransfered);
	// Sleep(1000);

	// --------------- THIRD FPGA --------------- //
	printf(" \n-------------- FPGA 3 --------------\n");
	bytesTransfered = 0;
	slaveAddress = SLAVE_3;
	bytesToTransfer = 1 + 258 + 3 + 1; 			// 1 memory address + 258 rgb values + 3 frequency bytes = 263 bytes
	buffer[bytesToTransfer];
	buffer[0] = 0x00;							/* Byte addressed inside FPGA */
	// printf("Memory address: %d\n",unsigned(buffer[0]));
	cout << "    R | G | B";
	for(int j=170;j<256;j++)
	{
		for (int i=0;i<3;i++)
		{
			buffer[(j-170)*3+i+1] = (uint8)stoi(dataList.at(j).at(i));
		}
		printf("\n%d: %d|%d|%d",j,unsigned(buffer[(j-170)*3+1]),unsigned(buffer[(j-170)*3+2]),unsigned(buffer[(j-170)*3+3]));
	}
	buffer[259] = (uint8)(frequency>>16);
	buffer[260] = (uint8)(frequency>>8);
	buffer[261] = (uint8)(frequency>>0);
	printf("\nFrequency: %d, \nFrequency (hex): %x %x %x",frequency,buffer[259],buffer[260],buffer[261]);

	if(sequence)
		sequence_hex = 0x04;
	if(test)
		test_hex = 0x02;
	buffer[262] = sequence_hex|test_hex;
	printf("\nConfiguration (hex):\n    Sequence: %x\n    Test: %x\n    Byte: %x",sequence_hex,test_hex,buffer[262]);

	printf("\nSlave address (hex): %x", slaveAddress);
	printf("\nBytes to transfer: %d", bytesToTransfer);
	i2cStatus = I2C_DeviceWrite(i2cHandle, slaveAddress, bytesToTransfer, buffer, &bytesTransfered, options);
	printf("\nTransfer status: %d", i2cStatus);
	printf("\nBytes transfered: %d", bytesTransfered);
	// Sleep(1000);

	// --------------- START --------------- //
	printf(" \n-------------- START --------------\n");
	bytesTransfered = 0;
	slaveAddress = SLAVE_1;
	bytesToTransfer = 2; 			// 1 memory address + 1 configuration byte = 2 bytes
	buffer[bytesToTransfer];
	buffer[0] = 0x02;				/* Byte addressed inside FPGA */
	if(sequence)
    	sequence_hex = 0x04;
	if(test)
		test_hex = 0x02;
	buffer[1] = sequence_hex|test_hex|0x01;
	printf("\nConfiguration (hex):\n    Sequence: %x\n    Test: %x\n    Byte: %x\n",sequence_hex,test_hex,buffer[1]);

	printf("\nSlave address (hex): %x", slaveAddress);
	printf("\nBytes to transfer: %d", bytesToTransfer);
	i2cStatus = I2C_DeviceWrite(i2cHandle, slaveAddress, bytesToTransfer, buffer, &bytesTransfered, options);
	printf("\nTransfer status: %d", i2cStatus);
	printf("\nBytes transfered: %d", bytesTransfered);
	return 0;
}

/*
Following are the special meanings of the FT_STATUS code returned in the context of I2C:
FT_OK = 0
FT_DEVICE_NOT_FOUND = 2
FT_IO_ERROR = 4
FT_INVALID_PARAMETER = 6
FT_FAILED_TO_WRITE_DEVICE = 10

FT_INVALID_HANDLE = 1
FT_DEVICE_NOT_OPENED = 3
FT_INSUFFICIENT_RESOURCES = 5
FT_INVALID_BAUD_RATE = 7
FT_DEVICE_NOT_OPENED_FOR_ERASE = 8
FT_DEVICE_NOT_OPENED_FOR_WRITE = 9
FT_EEPROM_READ_FAILED = 11
FT_EEPROM_WRITE_FAILED = 12
FT_EEPROM_ERASE_FAILED = 13
FT_EEPROM_NOT_PRESENT = 14
FT_EEPROM_NOT_PROGRAMMED = 15
FT_INVALID_ARGS = 16
FT_NOT_SUPPORTED = 17
FT_OTHER_ERROR = 18
*/