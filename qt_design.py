# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'leds.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
import sys
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import *
import subprocess as sub
import os

red = 0
green = 1
blue = 2

class MainWindow(QMainWindow):
    
    def __init__(self):
        super(MainWindow, self).__init__()
        self.resize(1500, 551)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setSizePolicy(sizePolicy)
        self.setMaximumSize(QtCore.QSize(1500, 551))
        self.setWindowTitle("PDK001")

        self.led_buttons = [0 for i in range(256)]
        self.led_buttons_names = []
        self.set_buttons_names = []
        self.rgb_values = [[0,0,0] for i in range(256)]

        self.setup()
        self.current_led = 0
        self.update_rgb_values()
        # QtCore.QMetaObject.connectSlotsByName(self)
   
    def setup(self):
        self.central_widget()
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.leds()
        spacerItem = QSpacerItem(20, 50, QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.options_layout = QHBoxLayout()
        self.options_layout.setObjectName("options_layout") 
        self.rgb()
        spacerItem1 = QSpacerItem(40, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)
        self.options_layout.addItem(spacerItem1)
        self.set_buttons()
        spacerItem2 = QSpacerItem(40, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)
        self.options_layout.addItem(spacerItem2)
        self.start_buttons()
        spacerItem3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.options_layout.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.options_layout)
        self.setCentralWidget(self.centralwidget)

    def start_buttons(self):
        self.start_buttons_layout = QVBoxLayout()
        self.start_buttons_layout.setObjectName("start_buttons_layout")

        self.pushButton_start_a = QPushButton(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.pushButton_start_a.setSizePolicy(sizePolicy)
        self.pushButton_start_a.setText("START A")
        self.pushButton_start_a.setObjectName("start_a")
        self.pushButton_start_a.clicked.connect(self.buttonClicked)
        self.start_buttons_layout.addWidget(self.pushButton_start_a)

        self.pushButton_start_b = QPushButton(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.pushButton_start_b.setSizePolicy(sizePolicy)
        self.pushButton_start_b.setText("START B")
        self.pushButton_start_b.setObjectName("start_b")
        self.pushButton_start_b.clicked.connect(self.buttonClicked)
        self.start_buttons_layout.addWidget(self.pushButton_start_b)

        self.pushButton_stop = QPushButton(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.pushButton_stop.setSizePolicy(sizePolicy)
        self.pushButton_stop.setText("STOP")
        self.pushButton_stop.setObjectName("stop")
        self.pushButton_stop.clicked.connect(self.buttonClicked)
        self.start_buttons_layout.addWidget(self.pushButton_stop)

        self.checkBox_sequence = QCheckBox(self.centralwidget)
        self.checkBox_sequence.setText("Sequence")
        self.checkBox_sequence.setObjectName("checkBox_sequence")
        self.start_buttons_layout.addWidget(self.checkBox_sequence)

        self.options_layout.addLayout(self.start_buttons_layout)

    def set_buttons(self):
        self.set_buttons_layout = QVBoxLayout()
        self.set_buttons_layout.setObjectName("set_buttons_layout")
        
        self.pushButton_set_rgb = QPushButton(self.centralwidget)
        self.pushButton_set_rgb.setEnabled(True)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Ignored)        
        self.pushButton_set_rgb.setSizePolicy(sizePolicy)
        self.pushButton_set_rgb.setText("SET RGB")
        self.pushButton_set_rgb.setObjectName("set_rgb")
        self.set_buttons_names.append("set_rgb")
        self.pushButton_set_rgb.clicked.connect(self.buttonClicked)
        self.set_buttons_layout.addWidget(self.pushButton_set_rgb)

        self.pushButton_set_all_r = QPushButton(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_set_all_r.sizePolicy().hasHeightForWidth())
        self.pushButton_set_all_r.setSizePolicy(sizePolicy)
        self.pushButton_set_all_r.setText("SET ALL R")
        self.pushButton_set_all_r.setObjectName("set_all_r")
        self.set_buttons_names.append("set_all_r")
        self.pushButton_set_all_r.clicked.connect(self.buttonClicked)
        self.set_buttons_layout.addWidget(self.pushButton_set_all_r)

        self.pushButton_set_al_g = QPushButton(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Ignored)
        self.pushButton_set_al_g.setSizePolicy(sizePolicy)
        self.pushButton_set_al_g.setText("SET ALL G")
        self.pushButton_set_al_g.setObjectName("set_all_g")
        self.set_buttons_names.append("set_all_g")
        self.pushButton_set_al_g.clicked.connect(self.buttonClicked)
        self.set_buttons_layout.addWidget(self.pushButton_set_al_g)

        self.pushButton_set_all_b = QPushButton(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Ignored)
        self.pushButton_set_all_b.setSizePolicy(sizePolicy)
        self.pushButton_set_all_b.setText("SET ALL B")
        self.pushButton_set_all_b.setObjectName("set_all_b")
        self.set_buttons_names.append("set_all_b")
        self.pushButton_set_all_b.clicked.connect(self.buttonClicked)
        self.set_buttons_layout.addWidget(self.pushButton_set_all_b)

        self.pushButton_set_all_rgb = QPushButton(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Ignored)
        self.pushButton_set_all_rgb.setSizePolicy(sizePolicy)
        self.pushButton_set_all_rgb.setText("SET ALL RGB")
        self.pushButton_set_all_rgb.setObjectName("set_all_rgb")
        self.set_buttons_names.append("set_all_rgb")
        self.pushButton_set_all_rgb.clicked.connect(self.buttonClicked)
        self.set_buttons_layout.addWidget(self.pushButton_set_all_rgb)

        self.options_layout.addLayout(self.set_buttons_layout)

    def rgb(self):
        self.rgb_layout = QFormLayout()
        self.rgb_layout.setFieldGrowthPolicy(QFormLayout.FieldsStayAtSizeHint)
        self.rgb_layout.setFormAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.rgb_layout.setObjectName("rgb_layout")

        font = QtGui.QFont()
        font.setPointSize(14)

        self.lable_id = QLabel(self.centralwidget)        
        self.lable_id.setFont(font)
        self.lable_id.setText("LED ID:")
        self.lable_id.setObjectName("lable_id")
        self.rgb_layout.setWidget(0, QFormLayout.LabelRole, self.lable_id)

        self.led_id = QLabel(self.centralwidget)
        self.led_id.setFont(font)
        self.led_id.setText("1")
        self.led_id.setObjectName("led_id")        
        self.rgb_layout.setWidget(0, QFormLayout.FieldRole, self.led_id)

        self.label_r = QLabel(self.centralwidget)
        self.label_r.setFont(font)
        self.label_r.setText("R:")
        self.label_r.setObjectName("label_r")
        self.rgb_layout.setWidget(1, QFormLayout.LabelRole, self.label_r)

        self.lineEdit_r = QLineEdit(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.lineEdit_r.setSizePolicy(sizePolicy)
        self.lineEdit_r.setMaximumSize(QtCore.QSize(100, 34))
        self.lineEdit_r.setFont(font)
        self.lineEdit_r.setObjectName("lineEdit_r")
        self.rgb_layout.setWidget(1, QFormLayout.FieldRole, self.lineEdit_r)

        self.label_g = QLabel(self.centralwidget)
        self.label_g.setFont(font)
        self.label_g.setText("G:")
        self.label_g.setObjectName("label_g")

        self.rgb_layout.setWidget(2, QFormLayout.LabelRole, self.label_g)
        self.lineEdit_g = QLineEdit(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.lineEdit_g.setSizePolicy(sizePolicy)
        self.lineEdit_g.setMaximumSize(QtCore.QSize(100, 34))
        self.lineEdit_g.setFont(font)
        self.lineEdit_g.setObjectName("lineEdit_g")
        self.rgb_layout.setWidget(2, QFormLayout.FieldRole, self.lineEdit_g)

        self.label_b = QLabel(self.centralwidget)
        self.label_b.setFont(font)
        self.label_b.setText("B:")
        self.label_b.setObjectName("label_b")

        self.rgb_layout.setWidget(3, QFormLayout.LabelRole, self.label_b)
        self.lineEdit_b = QLineEdit(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.lineEdit_b.setSizePolicy(sizePolicy)
        self.lineEdit_b.setMaximumSize(QtCore.QSize(100, 34))
        self.lineEdit_b.setFont(font)
        self.lineEdit_b.setObjectName("lineEdit_b")
        self.rgb_layout.setWidget(3, QFormLayout.FieldRole, self.lineEdit_b)

        self.label_frequency = QLabel(self.centralwidget)
        self.label_frequency.setFont(font)
        self.label_frequency.setText( "Frequency (Hz):")
        self.label_frequency.setObjectName("label_frequency")
        self.rgb_layout.setWidget(4, QFormLayout.LabelRole, self.label_frequency)

        self.lineEdit_frequency = QLineEdit(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)        
        self.lineEdit_frequency.setSizePolicy(sizePolicy)
        self.lineEdit_frequency.setMaximumSize(QtCore.QSize(100, 34))
        self.lineEdit_frequency.setFont(font)
        self.lineEdit_frequency.setText("10000")
        self.lineEdit_frequency.setObjectName("lineEdit_frequency")
        self.rgb_layout.setWidget(4, QFormLayout.FieldRole, self.lineEdit_frequency)

        self.options_layout.addLayout(self.rgb_layout)
     
    def central_widget(self):
        self.centralwidget = QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setMaximumSize(QtCore.QSize(1500, 500))
    
    """ Sets buttons representing LEDs """
    def leds(self):
        self.buttons_layout = QGridLayout()
        self.buttons_layout.setObjectName("buttons_layout")  

        cols = list(range(1,33))        
        for r in range(0,8):
            self.led_buttons_names.extend([str(8*c-r) for c in cols])
        positions = [(i,j) for i in range(8) for j in range(32)]

        for position, name in zip(positions, self.led_buttons_names):
            self.button = QPushButton(name)
            self.button.setObjectName(name)
            sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
            self.button.setSizePolicy(sizePolicy)
            self.button.setMaximumSize(QtCore.QSize(30, 30))
            self.button.setStyleSheet("background-color: rgba(0,0,0,90)")
            self.button.clicked.connect(self.buttonClicked)
            self.buttons_layout.addWidget(self.button, *position)
            self.led_buttons[int(name)-1] = self.button
        self.verticalLayout.addLayout(self.buttons_layout)

    def buttonClicked(self):
        # print("------------------------------")
        self.current_sender = self.sender()
        # print("Current LED start: " + str(self.current_led))
        # print ("Sender object name: " + self.current_sender.objectName())   
        if (self.current_sender.objectName() in self.led_buttons_names):
            self.led_clicked()
        elif (self.current_sender.objectName() == 'set_rgb'):
            self.set_rgb_clicked() 
        elif (self.current_sender.objectName() == 'set_all_r'):
            self.set_all_r_clicked()
        elif (self.current_sender.objectName() == 'set_all_g'):
            self.set_all_g_clicked()
        elif (self.current_sender.objectName() == 'set_all_b'):
            self.set_all_b_clicked()  
        elif (self.current_sender.objectName() == 'set_all_rgb'):
            self.set_all_rgb_clicked()
        elif (self.current_sender.objectName() == 'start_a'):
            self.start_a_clicked()
        elif (self.current_sender.objectName() == 'start_b'):
            self.start_b_clicked()
        # print("Current LED end: " + str(self.current_led))
        self.update_rgb_values()        

    def start_a_clicked(self):
        self.start_type = 0
        self.start_clicked()
    
    def start_b_clicked(self):
        self.start_type = 1
        self.start_clicked()
    
    """ Stores the LEDs and options data in data.csv and launches the C++ executable """
    def start_clicked(self):
        self.read_frequency()
        print("Saving LED values to txt file...")
        with open('data.csv',mode='w') as file:
            for rgb in self.rgb_values:
                file.write(str(rgb[0])+","+str(rgb[1])+","+str(rgb[2])+"\n")
                # file.write("\n")
            file.write(str(self.frequency)+"\n")
            file.write(str(self.start_type)+"\n")
            if(self.checkBox_sequence.isChecked()):
                file.write("1\n")
            else:
                file.write("0\n")
        print("Executing external program...")        
        sub.call(['main.exe',str(self.frequency)])
        print("Done.")

    def read_frequency(self):
        self.frequency = int(self.lineEdit_frequency.text())
        if (self.frequency > 350000):
            self.frequency = 350000
            self.lineEdit_frequency.setText(str(350000))
        elif (self.frequency < 10):
            self.frequency = 10
            self.lineEdit_frequency.setText(str(10))
        # print ("Frequency: " + str(self.frequency))

    """ Updates the color of the current LED button """
    def update_button_color(self):
        rgb = self.get_rgb_values()
        color = "background-color: rgba(%d,%d,%d,90)" % (rgb[red],rgb[green],rgb[blue])
        button = self.led_buttons[int(self.led_id.text())-1]
        print (self.led_id.text())
        button.setStyleSheet(color)        

    """ Updates id of current led """
    def led_clicked(self):        
        self.led_id.setText(self.current_sender.text())
        print ("RGB values: ",*self.rgb_values[int(self.current_sender.objectName())-1])
        self.current_led = int(self.current_sender.objectName())-1
        rgb = self.get_rgb_values()
        self.lineEdit_r.setText(str(rgb[red]))
        self.lineEdit_g.setText(str(rgb[green]))
        self.lineEdit_b.setText(str(rgb[blue]))

    """ Stores rgb values from lineEdits in database """
    def set_rgb_clicked(self): 
        self.rgb_values[int(self.led_id.text())-1] = self.read_rgb_values()
        self.update_button_color()
    
    """ Gets rgb values from database """
    def get_rgb_values(self):
        return [self.rgb_values[self.current_led][red],self.rgb_values[self.current_led][green],self.rgb_values[self.current_led][blue]]
    
    """ Loads rgb values from database to lineEdits """
    def update_rgb_values(self):
        rgb = self.get_rgb_values()
        self.lineEdit_r.setText(str(rgb[red]))
        self.lineEdit_g.setText(str(rgb[green]))
        self.lineEdit_b.setText(str(rgb[blue]))
    
    """ Reads rgb values from lineEdits, corrects overflows """
    def read_rgb_values(self):
        r = int(self.lineEdit_r.text())
        g = int(self.lineEdit_g.text())
        b = int(self.lineEdit_b.text())
        if (r > 255):
            r = 255
        if (g > 225):
            g = 255
        if (b > 255):
            b = 255
        return [r,g,b]

    """ Sets red values of all LEDs to current red value """
    def set_all_r_clicked(self):
        original_led = self.current_led
        rgb = self.read_rgb_values()
        for i in range(len(self.rgb_values)):
            self.rgb_values[i][red] = rgb[red]
        self.update_buttons_color()
        self.current_led = original_led
        self.update_rgb_values()

    def set_all_g_clicked(self):
        original_led = self.current_led
        rgb = self.read_rgb_values()
        for i in range(len(self.rgb_values)):
            self.rgb_values[i][green] = rgb[green]
        self.update_buttons_color()
        self.current_led = original_led
        self.update_rgb_values()

    def set_all_b_clicked(self):
        original_led = self.current_led
        rgb = self.read_rgb_values()
        for i in range(len(self.rgb_values)):
            self.rgb_values[i][blue] = rgb[blue]
        self.update_buttons_color()
        self.current_led = original_led
        self.update_rgb_values()

    def set_all_rgb_clicked(self):
        original_led = self.current_led
        rgb = self.read_rgb_values()                
        for i in range(len(self.rgb_values)):
            self.rgb_values[i] = rgb
        self.update_buttons_color()
        self.current_led = original_led
        self.update_rgb_values()

    """ Updates the color of the current LED button """
    def update_buttons_color(self):
        for name in self.led_buttons_names:
            self.current_led = int(name)-1
            rgb = self.get_rgb_values()
            color = "background-color: rgba(%d,%d,%d,90)" % (rgb[red],rgb[green],rgb[blue])
            button = self.led_buttons[self.current_led]
            button.setStyleSheet(color)

if __name__ == '__main__':    
    app = QApplication(sys.argv)
    ui = MainWindow()    
    ui.show()
    sys.exit(app.exec_())