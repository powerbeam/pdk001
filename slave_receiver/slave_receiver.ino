#include <Wire.h>

void setup()
{
  Wire.begin(1);  
  Wire.onReceive(receiveEvent); 
  Serial.begin(9600);
}

void loop()
{
  delay(100);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany)
{
  while(1 < Wire.available()) // loop through all but the last
  {
    int i2c_data = Wire.read(); // receive byte as a character
    Serial.print(i2c_data,DEC);         // print the character
    Serial.print(" ");
  }
  int x = Wire.read();    // receive byte as an integer
  Serial.print(x,DEC);         // print the integer
}
